
#include "QtJWT.hpp"

#include <QCryptographicHash>
#include <QJsonDocument>
#include <QStringBuilder>

#include <openssl/ec.h>
#include <openssl/ecdsa.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <openssl/obj_mac.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>

namespace QtJWT {

struct JSON {
    static QByteArray encode (const QJsonObject & json) {
        return (QJsonDocument { json }.toJson (QJsonDocument::Compact));
    }
    static QJsonObject decode (const QByteArray & str) {
        return (QJsonDocument::fromJson (str).object ());
    }
};

struct BASE64URL {
    static QByteArray encode (const QByteArray & raw) {
        return (raw.toBase64 (QByteArray::Base64UrlEncoding | QByteArray::OmitTrailingEquals));
    }
    static QByteArray decode (const QByteArray & b64u) {
        return (QByteArray::fromBase64 (b64u, (QByteArray::Base64UrlEncoding | QByteArray::OmitTrailingEquals)));
    }
};

namespace CRYPTO {

inline QByteArray sha256 (const QByteArray & bytes) {
    return QCryptographicHash::hash (bytes, QCryptographicHash::Algorithm::Sha256);
}

inline QByteArray makeByteArray (const quint8 * buffer, const unsigned int len) {
    return QByteArray { reinterpret_cast<const char *> (buffer), static_cast<int> (len) };
}

template<typename T> class PtrWithDTOR {
    using DTOR = std::function<void (T *)>;
    T *  _ptr  { nullptr };
    DTOR _dtor { nullptr };

public:
    explicit PtrWithDTOR (T * ptr, const DTOR & dtor) : _ptr { ptr }, _dtor { dtor } { }
    virtual ~PtrWithDTOR (void) {
        if (_ptr != nullptr && _dtor != nullptr) {
            _dtor (_ptr);
        }
    }
    inline T * ptr (void) { return _ptr; }
};

void handleOpenSslErrors (const char * functionName) {
    qCritical () << ">>> [JWT] OpenSSL function" << functionName << "failed, see error below :";
    ERR_print_errors_fp (stderr);
}

QByteArray signNone (const QByteArray & bytes, const QByteArray & key) {
    Q_UNUSED (bytes)
    Q_UNUSED (key)
    return { };
}

QByteArray signHS256 (const QByteArray & bytes, const QByteArray & key) {
    quint8 digest [EVP_MAX_MD_SIZE];
    unsigned int digestLength { 0 };
    HMAC (EVP_sha256 (), key.constData (), key.length (), reinterpret_cast<const quint8 *> (bytes.constData ()), bytes.length (), digest, &digestLength);
    return makeByteArray (digest, digestLength);
}

QByteArray signES256 (const QByteArray & bytes, const QByteArray & key) {
    PtrWithDTOR<BIO> bio { BIO_new_mem_buf (key.constData (), -1), &BIO_free };
    if (!bio.ptr ()) {
        qWarning () << ">>> [JWT] Error creating BIO for ECDSA private key";
        handleOpenSslErrors ("BIO_new_mem_buf");
        return { };
    }
    PtrWithDTOR<EC_KEY> ecKey { PEM_read_bio_ECPrivateKey (bio.ptr (), nullptr, nullptr, nullptr), &EC_KEY_free };
    if (!ecKey.ptr ()) {
        qWarning () << ">>> [JWT] Error reading ECDSA private key from PEM";
        handleOpenSslErrors ("PEM_read_bio_ECPrivateKey");
        return { };
    }
    const QByteArray hash { sha256 (bytes) };
    quint8 signature [ECDSA_size (ecKey.ptr ())];
    unsigned int signatureLength { 0 };
    if (ECDSA_sign (0, reinterpret_cast<const quint8 *> (hash.constData ()), hash.size (), signature, &signatureLength, ecKey.ptr ()) != 1) {
        qWarning () << ">>> [JWT] Error signing message with ECDSA";
        handleOpenSslErrors ("ECDSA_sign");
        return { };
    }
    return makeByteArray (signature, signatureLength);
}

QByteArray signRS256 (const QByteArray & bytes, const QByteArray & key) {
    PtrWithDTOR<BIO> bio { BIO_new_mem_buf (key.constData (), -1), &BIO_free };
    if (!bio.ptr ()) {
        qWarning () << ">>> [JWT] Error creating BIO for RSA private key";
        handleOpenSslErrors ("BIO_new_mem_buf");
        return { };
    }
    PtrWithDTOR<RSA> rsa { PEM_read_bio_RSAPrivateKey (bio.ptr (), nullptr, nullptr, nullptr), &RSA_free };
    if (!rsa.ptr ()) {
        qWarning () << ">>> [JWT] Error reading RSA private key from PEM";
        handleOpenSslErrors ("PEM_read_bio_RSAPrivateKey");
        return { };
    }
    const QByteArray hash { sha256 (bytes) };
    quint8 signature [RSA_size (rsa.ptr ())];
    unsigned int signatureLength { 0 };
    if (RSA_sign (NID_sha256, reinterpret_cast<const quint8 *> (hash.constData ()), hash.size (), signature, &signatureLength, rsa.ptr ()) != 1) {
        qWarning () << ">>> [JWT] Error signing message with RSA";
        handleOpenSslErrors ("RSA_sign");
        return { };
    }
    return makeByteArray (signature, signatureLength);
}

} // CRYPTO

static const QString ALGO_HS256 { QStringLiteral ("HS256") };
static const QString ALGO_ES256 { QStringLiteral ("ES256") };
static const QString ALGO_RS256 { QStringLiteral ("RS256") };
static const QString ALGO_NONE  { QStringLiteral ("none") };
static const QString JWT        { QStringLiteral ("JWT") };
static const QString TYP        { QStringLiteral ("typ") };
static const QString ALG        { QStringLiteral ("alg") };

inline Algo string2algo (const QString & str) {
    if      (str == ALGO_HS256) { return Algo::HMAC_SHA256; }
    else if (str == ALGO_ES256) { return Algo::ECDSA_P256_SHA256; }
    else if (str == ALGO_RS256) { return Algo::RSASSA_PKCS1_v1_5_SHA256; }
    else                        { return Algo::UNKNOWN; }
}

inline const QString & algo2string (const Algo algo) {
    switch (algo) {
        case Algo::HMAC_SHA256:              return ALGO_HS256;
        case Algo::ECDSA_P256_SHA256:        return ALGO_ES256;
        case Algo::RSASSA_PKCS1_v1_5_SHA256: return ALGO_RS256;
        default:                             return ALGO_NONE;
    }
}

QByteArray encode (const QJsonObject & values, const Algo algo, const QByteArray & key) {
    using SignFunc = std::function<QByteArray (const QByteArray &, const QByteArray &)>;
    static const QHash<Algo, SignFunc> CACHE {
        { Algo::HMAC_SHA256,              &CRYPTO::signHS256 },
        { Algo::ECDSA_P256_SHA256,        &CRYPTO::signES256 },
        { Algo::RSASSA_PKCS1_v1_5_SHA256, &CRYPTO::signRS256 },
    };
    const QJsonObject info {
        { TYP, JWT },
        { ALG, algo2string (algo) },
    };
    const QByteArray encodedHeader  { BASE64URL::encode (JSON::encode (info)) };
    const QByteArray encodedPayload { BASE64URL::encode (JSON::encode (values)) };
    QByteArray ret { (encodedHeader % '.' % encodedPayload) };
    if (algo != Algo::UNKNOWN) {
        if (key.isEmpty ()) {
            qWarning () << ">>> [JWT] Can't sign JWT without a key";
            return { };
        }
        const SignFunc func { CACHE.value (algo, &CRYPTO::signNone) };
        if (!func) {
            qWarning () << ">>> [JWT] Can't sign JWT with an unsupported algo";
            return { };
        }
        const QByteArray signature { func (ret, key) };
        if (signature.isEmpty ()) {
            qWarning () << ">>> [JWT] Couldn't sign JWT with given key and algo";
            return { };
        }
        ret += '.';
        ret += BASE64URL::encode (signature);
    }
    return ret;
}

QJsonObject decode (const QByteArray & jwt) {
    const QByteArrayList parts { jwt.split ('.') };
    if (parts.size () < 2 || parts.size () > 3) {
        qWarning () << ">>> [JWT] Can't decode JWT with bad format";
        return { };
    }
    const QByteArray encodedHeader { parts.at (0) };
    const QJsonObject info { JSON::decode (BASE64URL::decode (encodedHeader)) };
    if (info.value (ALG).isUndefined ()) {
        qWarning () << ">>> [JWT] Can't decode JWT with bad algo";
        return { };
    }
    const QByteArray encodedPayload { parts.at (1) };
    const QJsonObject payload { JSON::decode (BASE64URL::decode (encodedPayload)) };
    if (parts.size () == 3) {
        const QByteArray encodedSignature { parts.at (2) };
        // TODO : check signature ?
        Q_UNUSED (encodedSignature)
    }
    return payload;
}

}
