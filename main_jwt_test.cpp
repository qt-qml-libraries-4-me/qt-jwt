
#include <QCoreApplication>
#include <QDebug>

#include "QtJWT.hpp"

int main (int argc, char * argv []) {
    QCoreApplication app { argc, argv };
    const QByteArray HMAC_KEY {
        "This is a simple secret passphrase !"
    };
    const QJsonObject input {
        { "aud", "AUDIENCE" },
        { "sub", "SUBJECT" },
        { "iss", "ISSUER" },
        { "iat", "CREATION" },
        { "exp", "EXPIRATION" },
    };
    qInfo () << "INPUT=" << input;
    const QByteArray jwt { QtJWT::encode (input) };
    qInfo () << "JWT=" << jwt;
    const QJsonObject output { QtJWT::decode (jwt) };
    qInfo () << "OUTPUT=" << output;
    const QByteArray jwtHS256 { QtJWT::encode (input, QtJWT::Algo::HMAC_SHA256, HMAC_KEY) };
    qInfo () << "HMAC_SHA256=" << jwtHS256;
    return 0;
}
