
include ($$PWD/QtJWT.pri)

TEMPLATE = app

QT += core

SOURCES += $$PWD/main_jwt_test.cpp

windows {
    INCLUDEPATH += $$[QT_INSTALL_PREFIX]/../../Tools/OpenSSL/Win_x64/include
    LIBS        += $$[QT_INSTALL_PREFIX]/../../Tools/OpenSSL/Win_x64/lib/libcrypto.lib
}

unix {
    LIBS += -lcrypto
}
