#pragma once

#include <QByteArray>
#include <QJsonObject>

namespace QtJWT {

enum Algo {
    UNKNOWN,
    HMAC_SHA256,
    ECDSA_P256_SHA256,
    RSASSA_PKCS1_v1_5_SHA256,
};

QByteArray  encode (const QJsonObject & values, const Algo algo = Algo::UNKNOWN, const QByteArray & key = { });
QJsonObject decode (const QByteArray  & jwt);

}
